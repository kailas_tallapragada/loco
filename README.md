# Loco

Prerequisites:
node.js -> 8.0 or later
npm -> 6.3 or later

After cloning this repository, run the command "npm install" to install all dependencies.
Use the command npm start to run the server.
Use the command npm test to run the test cases.

const express = require('express');
const transactionHandler = require('./requesthandlers/transactionHandler');

const app = express();

/*
 * The below middleware is used to forward all requests starting with transactionservice to be forwarded to transactionHandler.js inside requesthandlers folder
 */
app.use('/transactionservice', transactionHandler);

const listener = app.listen(process.env.SERVER_PORT || 8080);

listener.on('listening', () => {
  console.log('Server listening on port ' + (process.env.SERVER_PORT || 8080));
});

listener.on('error', err => {
  console.log('There was an error while starting the server.\n' + err);
});

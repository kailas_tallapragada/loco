module.exports.Transaction = class {
  constructor(id, type, amount, parentId) {
    if (typeof id === 'number' && id >= 0) {
      this.id = id;
    } else {
      throw new Error('Transaction Id must be a non-negative number.');
    }
    if (typeof type === 'string' && type !== '') {
      this.type = type;
    } else {
      throw new Error('Transaction Type should be a non-empty string.');
    }
    if (typeof amount === 'number' && amount >= 0) {
      this.amount = amount;
    } else {
      throw new Error('Transaction Amount should be a non-negative number.');
    }
    if (typeof parentId === 'number' && parentId >= 0) {
      this.parentId = parentId;
    } else if (typeof parentId !== 'undefined' && parentId !== null) {
      throw new Error('Transaction Parent Id must be a non-negative number (or) undefined.');
    }
  }
};

const Transaction = require('./schema/transaction').Transaction;

const transactions = new Map();

function getTransactionChildren(parentId) {
  let resultTransactions = new Array();
  let transactionChildren = [...transactions.values()].filter(
    transaction => transaction.parentId === parentId
  );
  resultTransactions = resultTransactions.concat(transactionChildren);

  transactionChildren.forEach(transaction => {
    resultTransactions = resultTransactions.concat(getTransactionChildren(transaction.id));
  });

  return resultTransactions;
}

function throwTransactionIdTypeError() {
  throw new Error('Transaction Id must be a non-negative Integer.');
}

function throwParentTransactionIdTypeError() {
  throw new Error('Parent Transaction Id must be a non-negative Integer.');
}

module.exports.addTransaction = function(id, type, amount, parentId) {
  try {
    id = parseInt(id);
    if (!Number.isInteger(id)) throwTransactionIdTypeError();
  } catch (err) {
    throwTransactionIdTypeError();
  }

  try {
    if (parentId !== undefined && parentId !== null) {
      parentId = parseInt(parentId);
      if (!Number.isInteger(parentId)) throwParentTransactionIdTypeError();
    }
  } catch (err) {
    throwParentTransactionIdTypeError();
  }

  if (typeof id !== 'number' || id < 0) {
    throwTransactionIdTypeError();
  }
  if (transactions.has(id)) {
    throw new Error('A Transaction for the Transaction id "' + id + '" already exists.');
  }

  if (
    (typeof parentId !== 'number' || parentId < 0) &&
    typeof parentId !== 'undefined' &&
    parentId !== null
  ) {
    throwParentTransactionIdTypeError();
  }
  if (typeof parentId === 'number' && !transactions.has(parentId)) {
    throw new Error('The Parent Transaction for the Transaction id "' + id + '" does not exist.');
  }

  try {
    const transaction = new Transaction(id, type, amount, parentId);
    transactions.set(id, transaction);
    return id;
  } catch (err) {
    throw err;
  }
};

module.exports.getTransactionForId = function(id) {
  try {
    id = parseInt(id);
    if (!Number.isInteger(id)) throwTransactionIdTypeError();
  } catch (err) {
    throwTransactionIdTypeError();
  }

  if (typeof id !== 'number' || id < 0) {
    throwTransactionIdTypeError();
  }

  const transaction = transactions.get(id);
  if (transaction !== undefined) {
    return transaction;
  } else {
    throw new Error('There is no Transaction for the Transaction Id "' + id + '".');
  }
};

module.exports.getAllTransactionsOfType = function(type) {
  if (typeof type !== 'string' || type === '') {
    throw new Error('Transaction Type should be a non-empty string.');
  }

  const transactionsOfType = [...transactions.values()]
    .filter(transaction => transaction.type === type)
    .map(transaction => transaction.id);
  if (transactionsOfType.length > 0) {
    return transactionsOfType;
  } else {
    throw new Error('There are no Transactions for the Transaction Type "' + type + '".');
  }
};

module.exports.getTransitiveChildSum = function(id) {
  try {
    id = parseInt(id);
    if (!Number.isInteger(id)) throwTransactionIdTypeError();
  } catch (err) {
    throwTransactionIdTypeError();
  }

  let sum = 0;

  if (typeof id !== 'number' || id < 0) {
    throwTransactionIdTypeError();
  }

  const transaction = transactions.get(id);
  sum += transaction.amount;
  if (transaction === undefined) {
    throw new Error('There is no Transaction for the Transaction Id "' + id + '".');
  } else {
    getTransactionChildren(transaction.id).forEach(transaction => {
      sum += transaction.amount;
    });
    return sum;
  }
};

module.exports.clearAll = function() {
  transactions = new Map();
};

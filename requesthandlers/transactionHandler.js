const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const transactionCache = require('../modules/transactionCache');

/*
 * This function checks the validity of the input for transaction id, amount, type and parent_id and inserts it into cache if everything checks out.
 * Throws an error if there is a problem in the data type, values of the given inputs
 */
function addTransaction(req, res) {
  try {
    const params = req.params;
    const body = req.body;

    const transactionId = params && params.transaction_id;
    const amount = body && body.amount;
    const type = body && body.type;
    const parentId = body && body.parent_id;

    transactionCache.addTransaction(transactionId, type, amount, parentId);
    res.json({ status: 'ok' });
  } catch (err) {
    res.status(400).json({ status: 'error', message: err.message });
  }
}

/*
 * This function checks the validity of the input for transaction id and returns its data if it exists in the cache.
 * Throws an error if there is a problem in the data type, value of transaction id or if the id doesnt exist.
 */
function getTransactionForId(req, res) {
  try {
    const params = req.params;
    const transactionId = params && params.transaction_id;

    const transaction = transactionCache.getTransactionForId(transactionId);

    res.json({
      type: transaction.type,
      amount: transaction.amount,
      parent_id: transaction.parentId
    });
  } catch (err) {
    res.status(400).json({ status: 'error', message: err.message });
  }
}

/*
 * This function checks the validity of the input for transaction type and returns all ids of the type if any exist in the cache.
 * Throws an error if there is a problem in the data type, value of transaction type or if the type doesnt exist.
 */
function getAllTransactionsOfType(req, res) {
  try {
    const params = req.params;
    const transactionType = params && params.type;
    const transactions = transactionCache.getAllTransactionsOfType(transactionType);
    res.json(transactions);
  } catch (err) {
    res.status(400).json({ status: 'error', message: err.message });
  }
}

/*
 * This function checks the validity of the input for transaction id and returns sum of amount of all its childs and itself if id exists in the cache.
 * Throws an error if there is a problem in the data type, value of transaction id or if the id doesnt exist.
 */

function getTransitiveChildSum(req, res) {
  try {
    const params = req.params;
    const transactionId = params && params.transaction_id;

    const sum = transactionCache.getTransitiveChildSum(transactionId);
    res.json({ sum: sum });
  } catch (err) {
    res.status(400).json({ status: 'error', message: err.message });
  }
}

router.put('/transaction/:transaction_id', bodyParser.json(), addTransaction);

router.get('/transaction/:transaction_id', getTransactionForId);

router.get('/types/:type', getAllTransactionsOfType);

router.get('/sum/:transaction_id', getTransitiveChildSum);

module.exports = router;

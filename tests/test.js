require('../index');

const axios = require('axios');

const URL_PREFIX = 'http://localhost:8080/transactionservice';
const ADD_TRANSACTION_URL = '/transaction/';
const GET_TRANSACTION_URL = '/transaction/';
const GET_TYPE_URL = '/types/';
const GET_SUM_URL = '/sum/';

function createTransaction(id, type, amount, parentId) {
  return axios.put(URL_PREFIX + ADD_TRANSACTION_URL + id, {
    type: type,
    amount: amount,
    parent_id: parentId
  });
}

function getTransaction(id) {
  return axios.get(URL_PREFIX + GET_TRANSACTION_URL + id);
}

function getIdsForType(type) {
  return axios.get(URL_PREFIX + GET_TYPE_URL + type);
}

function getSumForId(id) {
  return axios.get(URL_PREFIX + GET_SUM_URL + id);
}

describe('Testing the successful insertion of transactions into cache', function() {
  it('Creates a transaction without a parent id and transaction id 1.', function(done) {
    createTransaction(1, 'parent', 1000).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.status === 'ok') {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Creates a transaction with a parent id 1 and transaction id 2.', function(done) {
    createTransaction(2, 'child_level_1', 2000, 1).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.status === 'ok') {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Creates a transaction with a parent id 2 and transaction id 3.', function(done) {
    createTransaction(3, 'child_level_2', 3000, 2).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.status === 'ok') {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Creates a transaction with a parent id 1 and transaction id 4.', function(done) {
    createTransaction(4, 'child_level_1', 4000, 1).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.status === 'ok') {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Creates a transaction with a parent id 4 and transaction id 5.', function(done) {
    createTransaction(5, 'child_level_2', 5000, 4).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.status === 'ok') {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
});

describe('Testing the failed insertion of transactions into cache.', function() {
  it('Tries to Create a transaction without a parent id and transaction id 1 but fails.', function(done) {
    createTransaction(1, 'parent', 1000).then(
      response => {
        done(new Error('Response did not match expected result.'));
      },
      err => {
        if (err && err.response && err.response.status === 400) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      }
    );
  });
  it('Tries to Create a transaction with a parent id 4 and transaction id 5 but fails.', function(done) {
    createTransaction(5, 'child_level_2', 1000, 4).then(
      response => {
        done(new Error('Response did not match expected result.'));
      },
      err => {
        if (err && err.response && err.response.status === 400) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      }
    );
  });
  it('Tries to Create a transaction with a invalid parent id.', function(done) {
    createTransaction(6, 'child_level_x', 1000, 10).then(
      response => {
        done(new Error('Response did not match expected result.'));
      },
      err => {
        if (err && err.response && err.response.status === 400) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      }
    );
  });
});

describe('Successfully getting data for transaction id.', function() {
  it('Successfully getting data for transaction id 1.', function(done) {
    getTransaction(1).then(
      response => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.type === 'parent' &&
          response.data.amount === 1000 &&
          response.data.parent_id === undefined
        ) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting data for transaction id 5.', function(done) {
    getTransaction(5).then(
      response => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.type === 'child_level_2' &&
          response.data.amount === 5000 &&
          response.data.parent_id === 4
        ) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
});

describe('Trying to get a invalid transaction id.', function() {
  it('Trying to get data for id 10 which is invalid.', function(done) {
    getTransaction(10).then(
      response => {
        done(new Error('Response did not match expected result.'));
      },
      err => {
        if (err && err.response && err.response.status === 400) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      }
    );
  });
});

describe('Successfully getting ids for type.', function() {
  it('Successfully getting ids for type parent.', function(done) {
    getIdsForType('parent').then(
      response => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.length === 1 &&
          response.data[0] === 1
        ) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting ids for type child_level_1.', function(done) {
    getIdsForType('child_level_1').then(
      response => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.length === 2 &&
          response.data[0] === 2 &&
          response.data[1] === 4
        ) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting ids for type child_level_2.', function(done) {
    getIdsForType('child_level_2').then(
      response => {
        if (
          response &&
          response.status === 200 &&
          response.data &&
          response.data.length === 2 &&
          response.data[0] === 3 &&
          response.data[1] === 5
        ) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
});

describe('Trying to get a invalid transaction type.', function() {
  it('Trying to get data for type xyz which is invalid.', function(done) {
    getIdsForType(10).then(
      response => {
        done(new Error('Response did not match expected result.'));
      },
      err => {
        if (err && err.response && err.response.status === 400) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      }
    );
  });
});

describe('Successfully getting sum for a given transaction id.', function() {
  it('Successfully getting sum for transaction id 1.', function(done) {
    getSumForId(1).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.sum === 15000) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting sum for transaction id 2.', function(done) {
    getSumForId(2).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.sum === 5000) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting sum for transaction id 3.', function(done) {
    getSumForId(3).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.sum === 3000) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting sum for transaction id 4.', function(done) {
    getSumForId(4).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.sum === 9000) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
  it('Successfully getting sum for transaction id 5.', function(done) {
    getSumForId(5).then(
      response => {
        if (response && response.status === 200 && response.data && response.data.sum === 5000) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      },
      err => {
        done(err || new Error());
      }
    );
  });
});

describe('Trying to get sum for a invalid transaction id.', function() {
  this.afterAll(() => console.log('\nPress CTRL + C to Exit.\n'));
  it('Trying to get sum for a invalid transaction id 10.', function(done) {
    getSumForId(10).then(
      response => {
        done(new Error('Response did not match expected result.'));
      },
      err => {
        if (err && err.response && err.response.status === 400) {
          done();
        } else {
          done(new Error('Response did not match expected result.'));
        }
      }
    );
  });
});
